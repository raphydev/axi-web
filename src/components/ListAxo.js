import {useMemo} from "react";

function ListAxo({mots}) {

    const processedMots = useMemo(() => {
        if (mots.length % 3 === 2) {
            return [...mots, ''];
        }
        return mots;
    }, [mots])

    const renderItem = (mot) => (mot
            ? <div className={'axo-item'}
                   key={mot.value}>
                <span>{mot.value}</span>
            </div>
            : <div className={'empty-item'} />
    )

    return (
        <div className="list-axo">
            <h1>Axo mots générés</h1>
            <div className={'items-container'}>
                {processedMots.map(renderItem)}
            </div>
        </div>
    );
}

export default ListAxo;
