import axios from "axios";
import {useEffect, useState} from "react";

function AxoGenerator({setMots}) {

    const [mot, setMot] = useState('');
    const [axoMot, setAxoMot] = useState('');

    useEffect(() => {
        if (axoMot && setMots){
            axios.post(`${process.env.REACT_APP_API_URL}/axomot`, {value: axoMot})
                .then((res) => setMots(mots => ([...mots, res.data])))
                .catch(console.error);
        }
    }, [axoMot, setMots])

    const onClickGenerate = () => {
        const formattedMot = mot[0].toUpperCase() + mot.substring(1).toLowerCase();
        setAxoMot(`Axo${formattedMot}`);
    }

    return (
        <div className="axo-generator">
            <div>
                <input type={'text'}
                       className={'axo-input'}
                       onChange={(event) => setMot(event.target.value)}
                       placeholder={'Sport'} />
                <button onClick={onClickGenerate}
                        className={'generer-btn'}>GÉNÉRER !
                </button>
                <span className={'axo-mot'}>{axoMot}</span>
            </div>
            <div>
            </div>
        </div>
    );
}

export default AxoGenerator;
