import './App.css';
import Header from "./components/Header";
import AxoGenerator from "./components/AxoGenerator";
import ListAxo from "./components/ListAxo";
import Footer from "./components/Footer";
import axios from "axios";
import {useEffect, useState} from "react";

function App() {

    const [mots, setMots] = useState([]);

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}/axomot`)
            .then((res) => setMots(res.data))
            .catch(console.error);
    }, [])

    return (
        <div className="app">
            <Header />
            <div className={'main-container'}>
                <AxoGenerator setMots={setMots}/>
                <ListAxo mots={mots}/>
            </div>
            <Footer />
        </div>
    );
}

export default App;
